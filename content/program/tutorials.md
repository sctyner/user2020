# Tutorial Sessions

## Morning

- **First steps in spatial data handling and visualization** &mdash; S. Rochette, D. Scott and J.
Nowosad
- **Predictive modeling with text using tidy data principles** &mdash; J. Silge and E. Hansen
- **So, you want to learn Python? An introduction to Python for the R lover** &mdash; S. Ellis
- **Application of Gaussian graphical models to metabolomics** &mdash; D. Scholtens and R. Balasubramanian
- **Periscope and CanvasXpress – Creating an enterprise-grade big-data visualization
application in a day** &mdash; C. Brett
- **Seamless R and C++ integration with Rcpp** &mdash; D. Eddelbuettel
- **Create and share reproducible code with R Markdown and workflowr** &mdash; J. Blischak
- **Causal inference in R** &mdash; L. D'Agostino McGowan and M. Barrett
- **Reproducible computation at scale with drake: hands-on practice with a machine
learning project** &mdash; W. Landau

## Afternoon

- **How green was my valley - Spatial analytics with PostgreSQL, PostGIS, R, and PL/R** &mdash; J. Conway
- **Creating beautiful data visualization in R: a ggplot2 crash course** &mdash; S. Tyner
- **Building interactive web applications with Dash for R** &mdash; R. Kyle
- **Easy Larger-than-RAM data manipulation with disk.frame** &mdash; ZJ Dai
5.R Markdown recipes &mdash; Y. Xie
- **Getting the most out of Git** &mdash; C. Gillespie and R. Davies
- **End-to-end machine learning with Metaflow: Going from prototype to production
with Netflix’s open source project for reproducible data science** &mdash; S. Goyal and B.
Galvin
- **Package development** &mdash; J. Hester and H. Wickham
- **What they forgot to teach you about teaching R** &mdash; M. Cetinkaya-Rundel
